package com.saminsheyda.batmanproject.application.presentation.view.customView

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView


class CollectionRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    interface OnSizeChangedListener {
        fun onSizeChange(list: CollectionRecyclerView, w: Int, h: Int, oldw: Int, oldh: Int)
    }

    var onSizeChangedListeners: List<OnSizeChangedListener> = mutableListOf()

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        onSizeChangedListeners.forEach { it.onSizeChange(this, w, h, oldw, oldh) }
    }
}