package com.saminsheyda.batmanproject.application.presentation.view.viewController.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import com.saminsheyda.batmanproject.R
import com.saminsheyda.batmanproject.application.presentation.viewModel.MovieDetailsViewModel
import com.saminsheyda.batmanproject.application.presentation.viewModel.ViewModelFactory
import com.saminsheyda.batmanproject.databinding.ActivityDetailsBinding

class DetailsActivity : BaseActivity<ActivityDetailsBinding>() {

    private val movieDetailsViewModel by lazy { ViewModelProvider(this, ViewModelFactory(application)).get(
        MovieDetailsViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        enableTranslucentStatusBar()
        setupActionBar()
        setupMovieDetails()
    }

    private fun setupActionBar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            title = ""
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    private fun setupMovieDetails() {
        binding.viewModel = movieDetailsViewModel
        val movieId = intent.getStringExtra(MOVIE_ID)
        movieId?.apply { movieDetailsViewModel.getMovieDetails(this) }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return true
    }
}