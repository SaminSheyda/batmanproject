package com.saminsheyda.batmanproject.application.presentation.view.viewController.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.GridLayoutManager
import com.saminsheyda.batmanproject.R
import com.saminsheyda.batmanproject.application.presentation.view.customView.CollectionRecyclerView
import com.saminsheyda.batmanproject.application.presentation.view.viewController.adapter.MovieListAdapter
import com.saminsheyda.batmanproject.application.presentation.view.viewController.adapter.MovieListItemDecorator
import com.saminsheyda.batmanproject.application.presentation.viewModel.SearchMovieViewModel
import com.saminsheyda.batmanproject.application.presentation.viewModel.ViewModelFactory
import com.saminsheyda.batmanproject.data.local.Model.MovieData
import com.saminsheyda.batmanproject.data.local.MoviesRoomDatabase
import com.saminsheyda.batmanproject.data.local.repository.MoviesRepository
import com.saminsheyda.batmanproject.databinding.ActivityMainBinding
import com.saminsheyda.batmanproject.interfaces.OnMoviePosterClickedListener
import com.saminsheyda.batmanproject.utils.ViewUtils
import com.saminsheyda.batmanproject.utils.checkInternetConnection
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.coroutineScope

const val MOVIE_ID = "MOVIE_ID"
const val DEFAULT_QUERY = "batman"

class MainActivity  : BaseActivity<ActivityMainBinding>() {

    val viewModel by lazy { ViewModelProvider(this, ViewModelFactory(application)).get(SearchMovieViewModel::class.java) }

    private val posterViewWidth by lazy { resources.getDimensionPixelSize(R.dimen.poster_width) }
    private val posterTransitionName by lazy { resources.getString(R.string.movie_poster_transition_name) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding.movieList.onSizeChangedListeners += onSizeChangedListener
//        if (viewModel.movieResults.value.isNullOrEmpty())


            viewModel.searchMovies(DEFAULT_QUERY)
        binding.viewmodel = viewModel

        viewModel.movieResults.observe(this, Observer {
            viewModel.savelocaldata()
        })

    }

    private fun setupRecyclerView() {
        val gridSpan = ViewUtils.calculateGridSpanCountForScreenWidth(
            list = binding.movieList,
            cellWidth = posterViewWidth.toFloat()
        )
        val horizontalPaddingBetweenPoster = ViewUtils.calculateHorizontalPaddingForList(
            binding.movieList,
            posterViewWidth.toFloat(),
            gridSpan
        )
        binding.movieList.apply {
            adapter = MovieListAdapter(onMoviePosterClickedListener)
            layoutManager = GridLayoutManager(this@MainActivity, gridSpan)
            addItemDecoration(MovieListItemDecorator(gridSpan, horizontalPaddingBetweenPoster))
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
      //  setupRecyclerView()
    }

    override fun onStop() {
        super.onStop()
    }


    private val onSizeChangedListener = object : CollectionRecyclerView.OnSizeChangedListener {
        override fun onSizeChange(
            list: CollectionRecyclerView,
            w: Int,
            h: Int,
            oldw: Int,
            oldh: Int
        ) {
            binding.movieList.onSizeChangedListeners -= this
            setupRecyclerView()
        }
    }

    private val onMoviePosterClickedListener = object : OnMoviePosterClickedListener {
        override fun onMoviePosterClicked(movieId: String, sharedTransitionView: View) {

            if (checkInternetConnection(application))
            {
                val activityOptionsCompat =
                    ActivityOptionsCompat
                        .makeSceneTransitionAnimation(
                            this@MainActivity,
                            sharedTransitionView,
                            posterTransitionName
                        )
                val intent = Intent(this@MainActivity, DetailsActivity::class.java)
                intent.putExtra(MOVIE_ID, movieId)
                startActivity(intent, activityOptionsCompat.toBundle())
            }
            else
            {
                val builder = AlertDialog.Builder(this@MainActivity)
                builder.setTitle("Error")
                builder.setMessage("Please check your internet connection to see the movie details.")
                builder.setIcon(android.R.drawable.ic_dialog_alert)
                builder.setPositiveButton("OK"){dialog, which ->
                    // Do something when user press the positive button
                }

                val dialog: AlertDialog = builder.create()
                dialog.show()

            }


        }
    }
}
