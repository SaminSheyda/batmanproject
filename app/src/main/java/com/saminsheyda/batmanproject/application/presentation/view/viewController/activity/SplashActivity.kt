package com.saminsheyda.batmanproject.application.presentation.view.viewController.activity

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.saminsheyda.batmanproject.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val imgSplashLogo = findViewById<ImageView>(R.id.imgSplashLogo)
        val scaleDownX = ObjectAnimator.ofFloat(imgSplashLogo, "scaleX", 1.5f)
        val scaleDownY = ObjectAnimator.ofFloat(imgSplashLogo, "scaleY", 1.5f)
        scaleDownY.duration = 3000
        scaleDownX.duration = 3000
        val animation1 = ObjectAnimator.ofFloat(
            imgSplashLogo,
            "alpha", 0f, 1f
        )
        animation1.duration = 3000
        val animSetXY = AnimatorSet()
        animSetXY.playTogether(animation1, scaleDownX, scaleDownY)
        animSetXY.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) { //                if(authenticate())
                run {
                        val intent = Intent(this@SplashActivity, MainActivity::class.java)
                        startActivity(intent)
                }
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })
        animSetXY.start()


    }


    override fun onPause() { // TODO Auto-generated method stub
        super.onPause()
        finish()
    }
}
