package com.saminsheyda.batmanproject.application.presentation.view.viewController.adapter
import android.view.View
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

abstract class BaseListAdapter<M : Any, VH : BaseViewHolder<M>>(private val diffCallback: DiffUtil.ItemCallback<M>) :
    ListAdapter<M, VH>(diffCallback) {

    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    override fun onViewRecycled(holder: VH) {
        super.onViewRecycled(holder)
        holder.unbind()
    }
}

abstract class BaseViewHolder<M : Any>(view: View) :
    RecyclerView.ViewHolder(view) {

    abstract fun bind(model: M)
    abstract fun unbind()
}