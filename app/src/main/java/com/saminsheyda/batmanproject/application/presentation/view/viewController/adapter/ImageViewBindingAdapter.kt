package com.saminsheyda.batmanproject.application.presentation.view.viewController.adapter

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.saminsheyda.batmanproject.R
import com.saminsheyda.batmanproject.utils.BlurTransformation


@BindingAdapter("app:imageUrl")
fun bindImage(imageView: ImageView, url: String?) {
    if (url == null || url.isEmpty()) {
        imageView.setImageResource(0)
        return
    }

    val options = RequestOptions().centerCrop()
    Glide.with(imageView.context).load(url).listener(object : RequestListener<Drawable> {
        override fun onLoadFailed(
            e: GlideException?,
            model: Any,
            target: Target<Drawable>,
            isFirstResource: Boolean
        ): Boolean {
            return false
        }

        override fun onResourceReady(
            resource: Drawable,
            model: Any,
            target: Target<Drawable>,
            dataSource: DataSource,
            isFirstResource: Boolean
        ): Boolean {
            return false
        }
    }).apply(options).into(imageView)
}

@BindingAdapter("blurImageUrl")
fun bindBlurImage(imageView: ImageView, url: String?) {
    if (url == null) {
        return
    }

    try {
        Glide.with(imageView.context)
            .asBitmap()
            .load(url)
            .transform(
                BlurTransformation(
                    imageView.context,
                    imageView.context.resources.getInteger(R.integer.background_blur_pixel_amount)
                )
            )
            .into(imageView)
    } catch (ex: IllegalArgumentException) {}
}