package com.saminsheyda.batmanproject.application.presentation.view.viewController.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.saminsheyda.batmanproject.databinding.ItemMovieBinding
import com.saminsheyda.batmanproject.domain.model.Movie
import com.saminsheyda.batmanproject.interfaces.OnMoviePosterClickedListener


class MovieListAdapter constructor(
    private val onMoviePosterClickedListener: OnMoviePosterClickedListener
) : BaseListAdapter<Movie, MovieListAdapter.ViewHolder>(TaskDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, onMoviePosterClickedListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.unbind()
    }

    class ViewHolder constructor(
        private val binding: ItemMovieBinding,
        private val onMoviePosterClickedListener: OnMoviePosterClickedListener
    ) : BaseViewHolder<Movie>(binding.root) {

        override fun bind(model: Movie) {
            binding.posterUrl = model.poster
            binding.movieTitle = model.title
            binding.root.setOnClickListener {
                model.imdbID?.apply {
                    onMoviePosterClickedListener.onMoviePosterClicked(this, binding.posterImage)
                }
            }
        }

        override fun unbind() {
            binding.root.setOnClickListener(null)
        }
    }
}

class TaskDiffCallback : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.imdbID == newItem.imdbID
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }
}