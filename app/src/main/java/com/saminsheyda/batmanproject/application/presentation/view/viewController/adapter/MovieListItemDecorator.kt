package com.saminsheyda.batmanproject.application.presentation.view.viewController.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class MovieListItemDecorator(private val gridSpan: Int, private val startMargin: Int) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        var index = parent.getChildAdapterPosition(view)
        outRect.left = startMargin
        outRect.top = startMargin
        if ((index + 1) % gridSpan == 0) {
            outRect.right = startMargin
        }
    }
}