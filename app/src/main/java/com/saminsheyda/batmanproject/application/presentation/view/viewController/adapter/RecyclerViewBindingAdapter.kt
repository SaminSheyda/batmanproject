package com.saminsheyda.batmanproject.application.presentation.view.viewController.adapter

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.saminsheyda.batmanproject.domain.model.Movie


@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Movie>?) {
    (listView.adapter as? MovieListAdapter)?.submitList(items)
}