package com.saminsheyda.batmanproject.application.presentation.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.saminsheyda.batmanproject.domain.model.Movie
import com.saminsheyda.batmanproject.domain.usecase.BaseUseCaseSubscriber
import com.saminsheyda.batmanproject.domain.usecase.GetMovieDetailsByIdUseCase

class MovieDetailsViewModel(
    private val getMovieDetailsByIdUseCase: GetMovieDetailsByIdUseCase
) : ViewModel() {

    val movieDetails = MutableLiveData<Movie>()
    val loadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()

    fun getMovieDetails(movieId: String) {
        if (movieId.isNotBlank()) {
            getMovieDetailsByIdUseCase.movieId = movieId
            getMovieDetailsByIdUseCase.execute(
                GetMovieDetailsByIdSubscriber(
                    movieDetails,
                    loadError,
                    loading
                )
            )
        }
    }

    class GetMovieDetailsByIdSubscriber(
        private val movieDetails: MutableLiveData<Movie>,
        private val loadError: MutableLiveData<Boolean>,
        private val loading: MutableLiveData<Boolean>
    ) : BaseUseCaseSubscriber<Movie>() {

        override fun onNext(result: Movie) {
            super.onNext(result)
            loadError.value = false
            loading.value = false
            movieDetails.value = result
        }
    }

    override fun onCleared() {
        super.onCleared()
        getMovieDetailsByIdUseCase.unsubscribe()
    }
}