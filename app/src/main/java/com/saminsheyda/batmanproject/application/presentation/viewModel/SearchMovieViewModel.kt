package com.saminsheyda.batmanproject.application.presentation.viewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.saminsheyda.batmanproject.data.local.Model.MovieData
import com.saminsheyda.batmanproject.data.local.MoviesRoomDatabase
import com.saminsheyda.batmanproject.data.local.repository.MoviesRepository
import com.saminsheyda.batmanproject.domain.model.Movie
import com.saminsheyda.batmanproject.domain.usecase.BaseUseCaseSubscriber
import com.saminsheyda.batmanproject.domain.usecase.SearchMoviesByTitleUseCase
import com.saminsheyda.batmanproject.utils.checkInternetConnection
import kotlinx.coroutines.launch


class SearchMovieViewModel(
    private val searchMoviesByTitleUseCase: SearchMoviesByTitleUseCase, application: Application
) : AndroidViewModel(application) {

    var myapplication: Application? = null
    val movieResults = MutableLiveData<List<Movie>>()
    val loadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()

    // The ViewModel maintains a reference to the repository to get data.
    private val repository: MoviesRepository
    // LiveData gives us updated words when they change.
    var allMovies: LiveData<List<MovieData>>? = null

    init {
        // Gets reference to WordDao from WordRoomDatabase to construct
        // the correct WordRepository.
        val moviesDao = MoviesRoomDatabase.getDatabase(application, viewModelScope).moviesDao()
        repository = MoviesRepository(moviesDao)
        myapplication = application
        allMovies = repository.allmovies
    }



    fun searchMovies(title: String) {

        if (checkInternetConnection(myapplication!!)) {
            Log.i("here", "hereeee")
            if (title.isNotBlank()) {
                Log.i("here", "hereeee2")
                searchMoviesByTitleUseCase.movieTitleQuery = title.trim()
                searchMoviesByTitleUseCase.execute(
                    SearchMoviesByTitleSubscriber(
                        movieResults,
                        loadError,
                        loading
                    )
                )
            }
        } else {
            getOfflineMovies()
        }
    }

    fun savelocaldata(){
        if (allMovies != null ||allMovies!!.value!!.isEmpty()) {
            viewModelScope.launch {
                movieResults.value!!.forEach {
                    var movieData = MovieData(it.title!!, it.poster!!, it.imdbID!!)
                    repository.insert(movieData)
                }
            }
        }
    }
    fun getOfflineMovies() {
        loading.value = true
        allMovies = repository.allmovies
        allMovies!!.observeForever {
            loading.value = false
            if (allMovies!!.value!!.isNotEmpty()) {
                loadError.value=false
                Log.i("here", "hereeee7")
                var list= ArrayList<Movie>()
                allMovies!!.value!!.forEach {
                    var movie=Movie(it.title,"","","","",
                        "","","","","","",
                        "","",it.poster,null,"","",
                        "",it.imdbID,"","","","",
                        "","")
                    list.add(movie)

                }
                movieResults.postValue(list)
            } else {
                Log.i("here", "hereeee8")
                loadError.value = true
            }
        }


//        allMovies!!.observeForever {
//            Log.i("here","hereeee6")
//            loading.value=false
//            if(it!=null && it.isNotEmpty())
//            {
//                Log.i("here","hereeee7")
//                movieResults.value=repository.allmovies.value
//            }
//            else
//            {
//                Log.i("here","hereeee8")
//                loadError.value=true
//            }
//
//        }

    }

    override fun onCleared() {
        super.onCleared()
        searchMoviesByTitleUseCase.unsubscribe()
    }

    class SearchMoviesByTitleSubscriber(
        private val movieResults: MutableLiveData<List<Movie>>,
        private val loadError: MutableLiveData<Boolean>,
        private val loading: MutableLiveData<Boolean>
    ) : BaseUseCaseSubscriber<List<Movie>>() {

        override fun onStart() {
            super.onStart()
            loadError.value = false
            loading.value = true
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            loadError.value = true
            loading.value = false
            movieResults.value = null
        }

        override fun onNext(result: List<Movie>) {
            super.onNext(result)
            loadError.value = false
            loading.value = false
            movieResults.value = result
            Log.i("here", "here " + result.get(0).imdbID)




        }


    }
}