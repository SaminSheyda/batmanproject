package com.saminsheyda.batmanproject.application.presentation.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.saminsheyda.batmanproject.domain.usecase.GetMovieDetailsByIdUseCase
import com.saminsheyda.batmanproject.domain.usecase.SearchMoviesByTitleUseCase


@Suppress("UNCHECKED_CAST")
class ViewModelFactory(application: Application) : ViewModelProvider.NewInstanceFactory() {
val myapplication:Application?=application
    override fun <T : ViewModel> create(modelClass: Class<T>) =
        with(modelClass) {
            when {
                isAssignableFrom(SearchMovieViewModel::class.java) ->
                    SearchMovieViewModel(SearchMoviesByTitleUseCase(),myapplication!!)
                isAssignableFrom(MovieDetailsViewModel::class.java) ->
                    MovieDetailsViewModel(GetMovieDetailsByIdUseCase())
                else ->
                    throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            }
        } as T
}
