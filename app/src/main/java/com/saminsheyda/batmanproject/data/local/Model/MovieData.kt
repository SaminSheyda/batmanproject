package com.saminsheyda.batmanproject.data.local.Model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.saminsheyda.batmanproject.domain.model.Rating

@Entity(tableName = "movies_table")
data class MovieData(
    @NonNull
   @ColumnInfo(name = "title")
    val title: String,
    @NonNull
   @ColumnInfo(name = "poster")
    val poster: String,
   @NonNull
    @PrimaryKey
    @ColumnInfo(name = "imdbID")
    @SerializedName("imdbID")
    val imdbID: String
    )