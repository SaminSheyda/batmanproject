package com.saminsheyda.batmanproject.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.saminsheyda.batmanproject.data.local.Model.MovieData

@Dao
interface MovieDao {

    // LiveData is a data holder class that can be observed within a given lifecycle.
    // Always holds/caches latest version of data. Notifies its active observers when the
    // data has changed. Since we are getting all the contents of the database,
    // we are notified whenever any of the database contents have changed.
    @Query("SELECT * from movies_table ORDER BY imdbID ASC")
    fun getAllMovies(): LiveData<List<MovieData>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(movieData: MovieData)

    @Query("DELETE FROM movies_table")
    suspend fun deleteAll()

    @Insert
    fun insertAll(movieData: List<MovieData?>?)
}