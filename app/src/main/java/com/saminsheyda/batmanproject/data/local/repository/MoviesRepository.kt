package com.saminsheyda.batmanproject.data.local.repository

import androidx.lifecycle.LiveData
import com.saminsheyda.batmanproject.data.local.Model.MovieData
import com.saminsheyda.batmanproject.data.local.MovieDao

class MoviesRepository(private val movieDao: MovieDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allmovies: LiveData<List<MovieData>> = movieDao.getAllMovies()

    suspend fun insertAll(movieData: List<MovieData?>?) {
        movieDao.insertAll(movieData)
    }
     suspend fun insert(movieData: MovieData) {
        movieDao.insert(movieData)
    }

}