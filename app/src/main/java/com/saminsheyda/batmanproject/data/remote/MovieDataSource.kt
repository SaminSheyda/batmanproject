package com.saminsheyda.batmanproject.data.remote


import com.saminsheyda.batmanproject.domain.model.Movie
import com.saminsheyda.batmanproject.domain.model.MovieSearchResult
import io.reactivex.Observable

interface MovieDataSource {

    fun searchMoviesByTitle(title: String): Observable<MovieSearchResult>

    fun getMovieDetailsById(movieId: String): Observable<Movie>
}