package com.saminsheyda.batmanproject.data.remote


import com.saminsheyda.batmanproject.domain.model.Movie
import com.saminsheyda.batmanproject.domain.model.MovieSearchResult
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

const val API_KEY = "3e974fca"

interface OMDBAPIClient {

    @GET("/")
    fun searchMovieByTitle(@Query("s") title: String, @Query("apikey") key: String = API_KEY): Observable<MovieSearchResult>

    @GET("/")
    fun getMovieDetailsById(
        @Query("i") id: String,
        @Query("apikey") key: String = API_KEY,
        @Query("plot") plot: String = "full"
    ): Observable<Movie>
}