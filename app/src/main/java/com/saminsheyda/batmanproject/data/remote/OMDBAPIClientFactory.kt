package com.saminsheyda.batmanproject.data.remote

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object OMDBAPIClientFactory {

    val client: OMDBAPIClient by lazy { getOMDBAPIClient() }

    private fun getOMDBAPIClient(): OMDBAPIClient {
        val gson =
            GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create()
        val retrofit = Retrofit.Builder().baseUrl("https://www.omdbapi.com")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()

        return retrofit.create(OMDBAPIClient::class.java)
    }
}