package com.saminsheyda.batmanproject.data.remote

import com.saminsheyda.batmanproject.domain.model.Movie
import com.saminsheyda.batmanproject.domain.model.MovieSearchResult
import io.reactivex.Observable

object RemoteOMDBMovieDataSource : MovieDataSource {

    override fun searchMoviesByTitle(title: String): Observable<MovieSearchResult> =
        OMDBAPIClientFactory.client.searchMovieByTitle(title)

    override fun getMovieDetailsById(movieId: String): Observable<Movie> =
        OMDBAPIClientFactory.client.getMovieDetailsById(movieId)
}
