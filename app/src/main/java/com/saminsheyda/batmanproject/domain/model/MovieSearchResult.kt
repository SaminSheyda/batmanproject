package com.saminsheyda.batmanproject.domain.model

import com.google.gson.annotations.SerializedName

data class MovieSearchResult constructor(val search: List<Movie>)

data class Movie constructor(
    val title: String?,
    val year: String?,
    val rated: String?,
    val released: String?,
    val runtime: String?,
    val genre: String?,
    val director: String?,
    val writer: String?,
    val actors: String?,
    val plot: String?,
    val language: String?,
    val country: String?,
    val awards: String?,
    val poster: String?,
    val ratings: List<Rating>?,
    val metascore: String?,
    val imdbRating: String?,
    val imdbvotes: String?,
    @SerializedName("imdbID") val imdbID: String?,
    val type: String?,
    val dvd: String?,
    val boxOffice: String?,
    val production: String?,
    val website: String?,
    val response: String?
)

data class Rating constructor(val source: String?, val value: String?)