package com.saminsheyda.batmanproject.domain.usecase

import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

abstract class BaseUseCase<T> {

    private val subscribeOn: Scheduler = Schedulers.io()
    private val observeOn: Scheduler = AndroidSchedulers.mainThread()
    private val disposables = CompositeDisposable()

    fun <O : Disposable> execute(disposableObserver: O) where O : Observer<T> {
        this.disposables.add(
            buildUseCaseObservable()
                .compose(this.applySchedulers())
                .subscribeWith(disposableObserver)
        )
    }

    fun unsubscribe() = disposables.clear()

    protected abstract fun buildUseCaseObservable(): Observable<T>

    private fun <X> applySchedulers(): ObservableTransformer<X, X> {
        return ObservableTransformer { observable ->
            observable
                .subscribeOn(this.subscribeOn)
                .unsubscribeOn(this.subscribeOn)
                .observeOn(this.observeOn)
        }
    }
}