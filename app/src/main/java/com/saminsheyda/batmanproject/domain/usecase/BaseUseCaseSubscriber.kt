package com.saminsheyda.batmanproject.domain.usecase


import io.reactivex.observers.DisposableObserver

abstract class BaseUseCaseSubscriber<T> : DisposableObserver<T>() {

    override fun onComplete() {}

    override fun onError(e: Throwable) {}

    override fun onNext(result: T) {}
}
