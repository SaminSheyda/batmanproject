package com.saminsheyda.batmanproject.domain.usecase


import com.saminsheyda.batmanproject.data.remote.RemoteOMDBMovieDataSource
import com.saminsheyda.batmanproject.domain.model.Movie
import io.reactivex.Observable

class GetMovieDetailsByIdUseCase : BaseUseCase<Movie>() {

    lateinit var movieId: String

    override fun buildUseCaseObservable(): Observable<Movie> = RemoteOMDBMovieDataSource.getMovieDetailsById(movieId)
}