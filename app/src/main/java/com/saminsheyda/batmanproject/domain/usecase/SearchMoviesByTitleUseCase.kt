package com.saminsheyda.batmanproject.domain.usecase

import com.saminsheyda.batmanproject.data.remote.RemoteOMDBMovieDataSource
import com.saminsheyda.batmanproject.domain.model.Movie
import com.saminsheyda.batmanproject.domain.usecase.BaseUseCase
import io.reactivex.Observable

class SearchMoviesByTitleUseCase : BaseUseCase<List<Movie>>() {

    lateinit var movieTitleQuery: String

    override fun buildUseCaseObservable(): Observable<List<Movie>> =
        RemoteOMDBMovieDataSource.searchMoviesByTitle(movieTitleQuery).flatMap { result ->
            Observable.fromArray(
                result.search
            )
        }
}