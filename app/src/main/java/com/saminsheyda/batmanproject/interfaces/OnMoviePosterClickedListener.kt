package com.saminsheyda.batmanproject.interfaces

import android.view.View

interface OnMoviePosterClickedListener {

    fun onMoviePosterClicked(movieId: String, sharedTransitionView: View)
}