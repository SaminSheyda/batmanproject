package com.saminsheyda.batmanproject.utils

import android.content.Context
import android.graphics.Bitmap
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import java.security.MessageDigest


class BlurTransformation(context: Context, private val radius: Int) : BitmapTransformation() {
    private val context: Context = context.applicationContext

    override fun transform(
        pool: BitmapPool,
        toTransform: Bitmap,
        outWidth: Int,
        outHeight: Int
    ): Bitmap? {
        return blur(toTransform)
    }

    fun blur(toTransform: Bitmap): Bitmap? {
        try {
            val blurredBitmap = toTransform.copy(Bitmap.Config.ARGB_8888, true)

            // Allocate memory for Renderscript to work with
            var rs: RenderScript? = null
            var input: Allocation? = null
            var output: Allocation? = null
            var script: ScriptIntrinsicBlur? = null
            try {
                rs = RenderScript.create(context)
                input = Allocation.createFromBitmap(
                    rs,
                    blurredBitmap,
                    Allocation.MipmapControl.MIPMAP_FULL,
                    Allocation.USAGE_SHARED
                )
                output = Allocation.createTyped(rs, input!!.type)

                // Load up an instance of the specific script that we want to use.
                script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
                script!!.setInput(input)

                // Set the blur radius
                script.setRadius(radius.toFloat())

                // Start the ScriptIntrinsicBlur
                script.forEach(output)

                // Copy the output to the blurred bitmap
                output!!.copyTo(blurredBitmap)
            } finally {
                script?.destroy()
                output?.destroy()
                input?.destroy()
                rs?.destroy()
            }

            return blurredBitmap
        } catch (ex: Throwable) {
            return null
        }

    }

    override fun updateDiskCacheKey(messageDigest: MessageDigest) {
        messageDigest.update("BlurTransformation(radius=$radius)".toByteArray())
    }
}