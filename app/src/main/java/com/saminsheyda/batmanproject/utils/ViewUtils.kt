package com.saminsheyda.batmanproject.utils
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.floor

class ViewUtils {

    companion object {
        @JvmStatic
        fun calculateGridSpanCountForScreenWidth(
            list: RecyclerView, startPadding: Int = 0, endPadding: Int = 0, cellWidth: Float
        ): Int {
            // Get list width minus padding
            val listWidth = (list.measuredWidth - startPadding - endPadding)

            // Work out maximum cards that can fit in the list width
            return floor((listWidth / cellWidth).toDouble()).toInt()
        }

        @JvmStatic
        fun calculateHorizontalPaddingForList(
            list: RecyclerView,
            cellWidth: Float,
            gridSpan: Int
        ): Int {
            if (gridSpan < 0) return 0
            return floor((list.measuredWidth - cellWidth * gridSpan) / (gridSpan + 1)).toInt()
        }
    }
}